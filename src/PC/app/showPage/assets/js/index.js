/**
 * index.js  主文件模块
 */

'use strict';

var Html5Plus
  , doc          = document
  , ws           = null
  , devicePR     = window.devicePixelRatio   // 设备像素比
  , winWidth     = document.body.clientWidth
  , winHeight    = document.body.clientHeight
  , colorDepth   = window.screen.colorDepth  // 位彩色
  , screenWidth  = window.screen.width  // 物理像素
  , screenHeight = window.screen.height
  , $picture     = $('#picture')
  , serverStatus // 服务器状态 Boolean类型
;

// 禁止选择
doc.oncontextmenu=function(){return false;};
window.plus ? plusReady() : doc.addEventListener('plusready', plusReady, false);
function plusReady(){
  // 设置应用是否保持唤醒（屏幕常亮）状态
  plus.device.setWakelock(true);
  // console.log('屏幕常亮：'+plus.device.isWakelock());
  // 获取当前窗口的WebviewObject对象
  ws = plus.webview.currentWebview();
  // 隐藏滚动条
  plus.webview.currentWebview().setStyle({scrollIndicator:'none'});
  // Android处理返回键
  plus.key.addEventListener('backbutton', function(){
    ws.close();
  }, false);
};

loadInfo(); //加载信息
// 判断是否是Html5Plus
(navigator.userAgent.indexOf('Html5Plus') < 0) ? Html5Plus = false : Html5Plus = true;
// console.log('Html5Plus: ' + Html5Plus);
var toolbarHtml = '<ul class="toolbar">'
                +   '<li class="tool-list" data-type="menu"><i class="iconfont icon-menu"></i></li>'
                +   '<li class="tool-list" data-type="refresh"><i class="iconfont icon-refresh"></i></li>'
                +   '<li class="tool-list" data-type="info"><i class="iconfont icon-info"></i></li>'
                +   (Html5Plus?'<li class="tool-list" data-type="exit"><i class="iconfont icon-exit"></i></li>':'')
                + '</ul>';

doc.getElementById('top-bar').innerHTML = toolbarHtml;

// 工具条事件
$(doc).on('tap', '#top-bar', function(e) {
  var $toolbar = $('.toolbar')
    , dataType
    , tapEvent
  ;
  if(!$toolbar.hasClass('show')){
    // 解决 显示除法点击
    setTimeout(function(){
      $toolbar.addClass('show');
    }, 50);
  }else{
    tapEvent = $(e.target);
    tapEvent.attr('data-type') ?
    dataType = tapEvent.attr('data-type') : dataType = $(e.target.parentNode).attr('data-type');
    var $info        = $('#info')
      , $pictureBox  = $('#pictureBox')
    ;
    switch(dataType){
      case 'menu':
        // 服务器是否正常
        if(serverStatus){
          // 判断列表是否显示
          if($pictureBox.length > 0){
            $pictureBox.remove();
          }else{
            pictureBox();
          };
          if($info.length > 0) $info.remove(); // 关闭信息框
        }
        else{
          toast('服务器链接失败，请重试'); // 请求失败提示
        };
        break;
      case 'refresh':
        // 服务器是否正常
        if(serverStatus){
          window.location.reload(); // 刷新页面
        }
        else{
          toast('服务器链接失败，请重试'); // 请求失败提示
        };
        break;
      case 'info':
        // 判断信息框是否显示
        if($info.length > 0){
          $info.remove(); // 关闭信息框
        }
        else{
          infoBox(); // 显示信息窗口
        };
        break;
      case 'exit':
        plus.device.setWakelock(false); // 设置应用是否保持唤醒（屏幕常亮）状态
        ws.close(); // 退出
        break;
    };
  };
  e.stopPropagation(); // 阻止冒泡
  return false;
});

// 点击其它地方隐藏工具栏
$(doc).on('tap swipe longTap', function(e) {
  var $toolbar = $('.toolbar');
  if (!$(e.target).is('#top-bar')
    && !$(e.target).is('.tool-list')
    && !$(e.target).is('.tool-list .iconfont')) {
    if($toolbar.hasClass('show')) $toolbar.removeClass('show');
  };
});

/**
 * 图片加载
 * @param {Object} imageInfo
 */
function loadImage(imageInfo){
  var imageUrl = imageInfo.Url;

  // console.log('后缀名：' + imageInfo.suffix);
  // 修改后psd文件缀名
  if(imageInfo.suffix === '.psd') imageUrl = imageUrl.replace(/.psd/i,'.png');
  // console.log('传递图片地址：' + imageUrl);
  var loader = new resLoader({
      baseUrl :'',
      resources : [imageUrl],
      onStart : function(total){
      // console.log('start:' + total);
      $('#loading').show();
      doc.getElementById('picture').src = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
    },
    onProgress : function(current, total){
      // console.log(current + '/' + total);
      var percent = current/total*100;
      // console.log(percent);
    },
    onComplete : function(total){
      $('#loading').hide();
      // 判断图片是否为动画
      if(imageInfo.suffix === '.gif' && imageInfo.animation){
        $picture.attr('src',imageUrl).width('initial');
        pictureMiddle();
      }
      else{
        $picture.attr('src',imageUrl).css({left:'0',top:'0',width:'100%'});
      };
      touchEvent();  // 重新注册事件
    }
  });
  loader.start();
};

// 图片列表盒子
function pictureBox(){
  var pictureHtml = []
    , data        = JSON.parse(localStorage.getItem('data')).fileList
  ;
  pictureHtml.push('<div id="pictureBox"><ul class="pictureList">' + liHtml(data) + '</ul></div>');

  /**
   * lihtml结构
   * @param {Object} data
   */
  function liHtml(data){
    var localImg = localStorage.getItem('imgData') //本地数据
      , oli      = []
    ;

    for(var i in data){
      if(JSON.parse(localImg).name == data[i].name){
        oli.push('<li class="active"><p>' + data[i].name +'</p><i class="iconfont icon-point-sure"></i></li>');
      }else{
        oli.push('<li><p>' + data[i].name +'</p></li>');
      };
    };
    // 判断本地数据是否存在，设置第一张图片显示；
    if(!localImg){
      oli[0] = '<li class="active"><p>' + data[0].name +'</p><i class="iconfont icon-point-sure"></i></li>';
      localStorage.setItem('imgData',JSON.stringify(data[0]));
    };
    return oli.join('');
  };

  if(!$('#pictureBox').length)$('#content').before(pictureHtml.join(''));
};

// 图片文件列表
$(doc).on('tap','#pictureBox li p',function(){
  var $li  = $(this).parent()
    , data = JSON.parse(localStorage.getItem('data')).fileList // 本地数据
  ;
  if($li.hasClass('active')){
    $(this).siblings('i.iconfont').remove();
    // console.log($li.index());
    loadImage(data[$li.index()]); // 加载图片
    localStorage.setItem('imgData',JSON.stringify(data[$li.index()])); // 保存当前加载图片信息
    $li.append('<i class="iconfont icon-point-sure"></i>').parents('#pictureBox').remove();
  }else{
    $('#pictureBox li').removeClass('active');
    $li.addClass('active');
    return false;
  };
});

// 图片错误显示
function nofind(element){
  var img = event.srcElement || event.target ;//获取img对象，火狐是event.target ，IE及谷歌其他是event.srcElement
    // console.dir(img);
    img.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAACCCAMAAAC93eDPAAAAnFBMVEX///9mbXVoa3CmqrTp6uzT1dq8v8f09fb6+vrv8PHIytC3usKGiY3d3+PZ296xtb2sr7n29vducXaDhYnOz9Xk5efCxcymqKuWmJyNkZeIio/z8/N2eX1qbnWdnqKanJ+Wmp6Lj5RwdXvs7e3b3d/Lzc6SlJh0dnvi4uPR0tW3ur60triusrahoqbo6el1e4PAwcOsrrF7fYLDxMYR5CHpAAADqklEQVR42uzVy6rCMBCA4ckghEIpxC6a3msrFsWj4Ps/3GmajZtREiYbmX+TzSw+0iQFSZIkSQqsmJ6KtedUQFA3laBb0B70KkH9OYBwUkmqAwizStIcQFCJEoIQhCAEIbAQ6msGLGXXOo5wB8buMYQaWDtFEM7AWhFBMMCaiSAAc0IQghCEwE7Ixq4y26qrdWiBTA9dpd/GOAkWERsDOt/WEcj+EDHXfmxgJhzRGZbcLSuQregMh32sYyZc0LVD0AKZfRubmAltiXt+o6n8F/A1hpPgDYSAMOSG/1I+jn6TF/jYgRhjIOic2GBqFzQHgf7ItIA4MgwE+qjTY5aZ0ONW+SjdMn97F5bGj/G/C6X298J+fR2NM4zMhMz2k3Z384I2A7J2eFVuzFSvof29P6UQhCAEIQjhn127XUEQhgIwPA6JChbMD0zRxI+ZtQzN+7+3wCil0Aw8prj3BnzcGPuxsxjClZWBpvkAYGihXSXytARq5h68F/JsKkLGHejIy02KTqCFAb3p9gaVYNk6fC9IsAgKC6GVceAsq3c/zrbVJfWgyWUoBKa3Pl+e6OcRKYJjsx8SAuH1l2mhkq4k23sqxycoUHc7q6Q/9jgvPsIquAB6viMDssoIIEUgbPZcJgOjPIjXcUcIgiAIgiCsk0DJqCnLfDD+87N5hDM8EP1CxhmhcJY17ziDqc85zL6K7u2Uy5KEIAxFU5UHgRTCQvn/T53hMZba3ZvB6pVnIWqJnHCjDw8P/4IcfAHHZ9qiyADH0aV2WxiAECqs9fAHzRWKcgTpnUJMcFbQZAqAJo3Mkwrt5RE67xWCMVPkJMwBW9E+m9aHG3KDQsC1p/BeQbOIxFVyEjGU5rsFuFNhGBh+UIgJjkEMbgxCQy41Wca4B3FiSysRHRW0X2PgRppV8KIQbJPsYVcIdMCZICLhYJzXuQoN725oxy0jLu7ajoN+Ww9SfdoqO3FyF5aSE/tomIr/rABAAz/MI//Sw6A5hWirX0gBXAw6FJIM6nJeBE1WwIF0hQb1CfNBuISYCv2V67FwI7RL12qNMFbjoUD9icCVWQWq4xL2dtyQLjW2EReqhKEQ2/c4NmxSwSEm4ej3tQLqq4K/BuH7TyvpHUEoLatkRO4KmgtcFbYQLkFwnWgGZjqvMHCb7woF6UUhm54UIuYNKGUPLieaVThDsOAKFwVCUzgFsZiPgkWruqBNtiMdqaGLXhWc6KUdQaEUgk60AN+BtPt4eHh4mOAHhMA67J2tWKYAAAAASUVORK5CYII=";
    $(element).width('initial');
    pictureMiddle();
    img.onerror = null;
}

// 信息窗口
function infoBox(){
  if(screenWidth === winWidth){
    screenWidth  = Math.round(screenWidth*devicePR);
    screenHeight = Math.round(screenHeight*devicePR);
  };

  var $img      = doc.getElementById('picture')
    , img       = JSON.parse(localStorage.getItem('imgData'))
    , infohHtml = '<div id="info">'
                +   '<div class="title-bar">'
                +     '<span class="title">信息</span>'
                +     '<i class="icon-close iconfont" title="关闭"></i>'
                +   '</div>'
                +   '<div class="content">'
                +     '<ul class="list">'
                +       '<li><b class="name">名称 : </b>' + img.name + '</li>'
                +       '<li><b class="name">图片类型 : </b>' + img.suffix + '</li>'
                +       '<li><b class="name">图片尺寸 : </b>' + $img.offsetWidth + '&times;' + $img.offsetHeight + '(宽&times;高 )</li>'
                +       '<li><b class="name">尺寸说明 : </b>当前为' + sizeName($img) + '</li>'
                +       '<li><b class="name">修改时间 : </b>' + img.ctime + '</li>'
                +       '<li><b class="name">屏幕色彩 : </b>' + colorDepth + '位色</li>'
                +       '<li><b class="name">屏幕像素比 : </b>' + devicePR + '像素/英寸</li>'
                +       '<li><b class="name">屏幕分辨率 : </b>' + winWidth + '&times;' + winHeight + ' (宽&times;高)</li>'
                +       '<li><b class="name">屏幕物理分辨率 : </b>' + screenWidth + '&times;' + screenHeight + '(宽&times;高)</li>'
                +     '</ul>'
                +   '</div>'
                + '</div>'
  ;

  /**
   * 图片尺寸名称
   * @param {Object} img
   */
  function sizeName(img){
    if(img.style.width == 'initial'){
      return '图片实际尺寸';
    }
    else if(img.style.width == '100%'){
      return '图片设备等宽尺寸';
    }
    else{
      return '图片设备尺寸';
    };
  };

  if(!$('#info').length)$('#content').before(infohHtml);
};

// 关闭信息窗口
$(doc).on('tap','#info .icon-close',function(){
  $('#info').remove();
});

// 加载信息
function loadInfo(){
  $.ajax({
    type:"post",
    url:'/',
    dataType:'json',
    timeout:500,
    success:function(data){
      // console.info(data.fileList.length);
      if(data.fileList.length > 0 ){
        var ProjectAdd = localStorage.getItem('data') ?
        JSON.parse(localStorage.getItem('data')).ProjectAdd : false;
        // 判断本地缓存项目地址是否改变
        if(!ProjectAdd || ProjectAdd !== data.ProjectAdd){
          localStorage.setItem('data',JSON.stringify(data));  // 保存数据到本地
          localStorage.setItem('imgData',JSON.stringify(data.fileList[0]));  // 保存数据到本地
        }
        else{
          localStorage.setItem('data',JSON.stringify(data));  // 保存数据到本地
        };
        document.body.style.backgroundColor = data.bgColor; // 修改背景色
        loadImage(JSON.parse(localStorage.getItem('imgData'))); // 加载图片
        monitorChange();  // 启动轮询
      }
      else{
        toast('文件夹目录没有图片'); // 请求失败提示
      };
    },
    error:function(xhr,type){
      toast('服务器链接失败，请重试'); // 请求失败提示
    },
    async:true
  });
};

// 检测修改
function monitorChange(){
  $.ajax({
    type : "post",
    url : '/timestamp',
    dataType :'json',
    timeout : 500,
    success : function(data){
      var localData = JSON.parse(localStorage.getItem('data'));
      serverStatus = true; // 服务器正常
      // console.info(data.timestamp, localData.timestamp);
      if(data.timestamp !== localData.timestamp){
        window.location.reload(); // 刷新页面
      };
      createTimer(1500); // 创建轮询
    },
    error: function(xhr, type){
      serverStatus = false; // 服务器掉线
      createTimer(1500); // 创建轮询
    },
    async:true
  });
};

// 创建定时
function createTimer(time){
  setTimeout(function(){
    monitorChange();
  },time);
};

// 自动消失提示框
function toast(text){
  if(Html5Plus){
    plus.nativeUI.toast(text);
  }
  else{
    var toast = $('.toast')
      , timer = null
    ;
    if(toast.css('display') == 'none'){
      toast.html(text).show();
      timer = setInterval(function(){
        toast.hide();
        clearInterval(timer);
      },2500);
    }
    else{
      return;
    };
  };
};

//////////////////////////////////////////
function touchEvent(){
  // 图片手势事件
  var start_left
    , start_top
  ;
  util.toucher(doc.getElementById('picture'))
  .on('swipeStart',function(e){
    // console.log('开始滑动img');
    start_left = parseInt(this.style.left) || 0;
    start_top  = parseInt(this.style.top) || 0;
    this.style.transition = 'none';
  })
  .on('swipe',function(e){
    var newsLeft = (start_left + e.moveX) + 'px'
      , newsTop  = (start_top + e.moveY) + 'px'
    ;
    if(this.style.width == '100%'){
      $(this).css({top:newsTop,left:'0'});
      return false;
    }else{
      $(this).css({top:newsTop,left:newsLeft});
      return false;
    };
  })
  .on('swipeEnd',function(e){
    this.style.transition = '.1s';
    return false;
  });

  // 双击图片切换显示图片大小
  util.toucher(doc.getElementById('picture')).on('doubleTap',function(){
    // console.log('双击了img');
    var imgWidth = this.style.width;
    // 默认initial尺寸
    if(imgWidth == '100%'){
      this.style.width = 'initial';
//    return false;
    };
    // 原始尺寸
    if(imgWidth == 'initial'){
      var PixelX = Math.round(this.offsetWidth/devicePR)
        , PixelY = Math.round(this.offsetHeight/devicePR)
      ;
      $(this).css({top:'0',left:'0'});
      // 图片实际宽高小于100直接显示屏幕宽度，防止显示过小无法预览；
      if(PixelX < 100 || PixelY < 100){
        this.style.width = '100%';
//      return false;
      }
      else{
        this.style.width = PixelX +'px';
//      return false;
      };
    };
    // 屏幕等宽尺寸
    if(imgWidth != '100%' && imgWidth != 'initial'){
      $(this).css({top:'0',left:'0',width:'100%'});
//    return false;
    };
  });
  // #content 双击背景让图片显示中间
  $(doc).on('doubleTap',function(e){
    // console.log('双击了document');
    pictureMiddle();
//  return false;
  });
};

// 图片居中
function pictureMiddle(){
  $picture.css({
    top: (winHeight - $picture.height())/2 + 'px',
    left: (winWidth - $picture.width())/2 + 'px'
  });
}
