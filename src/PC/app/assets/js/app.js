/**
 * app.js  服务模块
 */

'use strict';

var fs         = require('fs')
  , path       = require('path')
  , express    = require('express')
  , app        = express()
  , api        = require('./api') // 公共函数模块
  , walkDir    = require('./walkDir') // 遍历目录
;

// showPage设置为静态路径
app.use('/', express.static(path.resolve(__dirname, '../../showPage')));

// 设置引入字体文件路径
app.use('/iconfont', express.static(path.resolve(__dirname, '../iconfont')));

// 发送时间戳
app.post('/timestamp', function(req, res){
  res.json({
    timestamp : api.getlocalStorage('app-info', 'timestamp')
  })
});

module.exports.appStart = function(Port,FileAdd){
  // 指定路由为静态路径
  app.use('/img', express.static(FileAdd));

  // post数据
  app.post('/', function(req, res){
    // 发送信息
    res.json({
      timestamp  : api.getlocalStorage('app-info', 'timestamp'),
      bgColor    : api.getlocalStorage('app-info', 'bgColor'),
      ProjectAdd : FileAdd,
      fileList   : walkDir.picture(FileAdd)
    })
  });

  app.listen(Port); // 监听端口

  setTimeout(function(){
    walkDir.PreConvert(FileAdd);
  }, 500);
};
