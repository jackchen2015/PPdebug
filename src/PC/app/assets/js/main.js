/**
 * main.js 主文件模块
 */

'use strict';

var gui = require('nw.gui');
    global.gui = gui;
    global.mainWindow = gui.Window.get();
    global.jQuery = jQuery;

var api       = require('./assets/js/api')
  , winEvents = require('./assets/js/winEvents')
;

// 初始化   /////////////////////////////////////////

var times       = null  // 用于icon-point-browser，单击双击事件
  , doc         = $(document)
  , $startBtn   = $('.start-btn')
  , $ipSelect   = $('.ip-select')
  , $ipInput    = $('.ip-input')
  , $portInput  = $('.port-input')
  , $addrInput  = $('.addr-input')
  , $colorInput = $('.color-input')
  , $colorBox   = $('.color-box')
;

var init = (function(){
  // 填写ip
  var ipList  = []
    , localIP = api.getlocalStorage('app-info', 'IP') // 缓存 IP
    , getIp   = api.getIPAdress() // 获取 IP 地址
    , isIP    = false // IP 是否有效
  ;
  
  if(getIp.length > 0){
    getIp.forEach(function(i){
      ipList.push('<li data-type="'+ i +'">'+ i +'</li>');
      // 判断缓存 IP 是否有效
      localIP == i ? isIP = true : isIP = false;
    });
    $ipSelect.html(ipList);
    isIP ? $ipInput.val(localIP) : $ipInput.val(getIp[0]);
  }
  // 未获取到ip处理
  else{
    $('.icon-point-edit').attr('title','确定')
      .removeClass('icon-point-edit')
      .addClass('icon-point-sure')
      .next('.ip-input').removeAttr('readonly');
  };

  // 填写bgColor
  var localbgColor = api.getlocalStorage('app-info', 'bgColor');
  localbgColor !== '' && $colorInput.val(localbgColor);

  // 填写Port
  var localPort = api.getlocalStorage('app-info', 'Port');
  localPort !== '' && $portInput.val(localPort);

  // 地址addr
  var localaddr = api.getlocalStorage('app-info', 'addr');
  localaddr !== '' && $addrInput.val(localaddr);

  // 获取版本
  $('.version').attr('title' ,'【Beta版】\n 程序版本：' + api.getVersion() + '\n 点击可查看是否有新版本');
})();

// 绑定事件   /////////////////////////////////////////

// 打开下载地址
doc.on('click', '.version', function(){
  api.openDownload();
});

// title-bar单击事件(检查升级,最小化,关闭按钮)
doc.on('click','.title-bar',function(e){
  var clickEvent = $(e.target);
  if(clickEvent.hasClass('icon-update')){
    // 检查升级按钮
    // console.log('icon-update');
    return;
  }
  // 最小化按钮
  else if(clickEvent.hasClass('icon-win-minimize')){
    // console.log('icon-win-minimize');
    winEvents.winMinimize(false);
    return;
  }
  // 关闭按钮
  else if(clickEvent.hasClass('icon-win-close')){
    // console.log('icon-win-close');
    winEvents.winClose();
  };
});

// 修改调试端背景色
doc.on('click', '.tool-bar .icon-point-color', function(){
  $colorBox.fadeIn(); // 显示color-box 盒子
});

// 颜色修改按钮
doc.on('click', '.color-box .color-btn', function(){
  // console.log('color-btn');
  var bgColor  = api.getlocalStorage('app-info', 'bgColor')
    , newColor = $colorInput.val()
  ;
  // 判断颜色是否被修改
  if(bgColor !== newColor){
    api.setlocalStorage('app-info', 'bgColor', newColor); // 修改颜色
    api.setlocalStorage('app-info', 'timestamp', api.timestamp()); // 刷新
  };
  $colorBox.fadeOut();
});

// 复制地址
$('.tool-bar .icon-point-browser').on({
  click:function(){
    clearTimeout(times);
    times=setTimeout(function(){
      // console.log('click');
      // 获取系统剪贴板
      var clipboard = global.gui.Clipboard.get();
      if($startBtn.attr('data-type') === '1'){
        // 剪切板写入信息
        clipboard.set(Website, 'text');
        // console.log(clipboard.get('text'));

        // 打印读取剪切板信息
        Website != clipboard.get('text') || api.toast('复制成功');
      }
      else{
        api.toast('请启动后再操作');
      };
    }, 300);
  },
  dblclick:function(){
    clearTimeout(times);
    // console.log('dblclick', Website);
    if($startBtn.attr('data-type') === '1'){
      global.gui.Shell.openExternal(Website);
    }
    else{
      api.toast('请启动后再操作');
    };
  }
});

// 刷新调试端
doc.on('click', '.tool-bar .icon-point-refresh', function(){
  if($startBtn.attr('data-type') === '1'){
    api.setlocalStorage('app-info', 'timestamp', api.timestamp());
  }
  else{
    api.toast('请启动后再操作');
  };
});

// 打开关于PPdebug
doc.on('click', '.tool-bar .icon-point-about', function(){
  api.about(); // 打开关于PPdebug
})

// 表单事件
doc.on('click', '.content', function(e){
  if($startBtn.attr('data-type') === '1') return; // 判断是否启动
  var clickEvent = $(e.target)
    , ipVal      = $ipInput.val()
  ;

  if(clickEvent.hasClass('icon-point-sure')){
    if(ipVal == '' || !api.isIP(ipVal)){
      api.toast('IP 地址填写不正确');
      return;
    };
    // 保存ip
    // showQrcode();  // 更新二维码
    $('.icon-point-sure').attr('title', '编辑')
      .removeClass('icon-point-sure')
      .addClass('icon-point-edit')
      .next('.ip-input').attr('readonly', 'readonly')
    ;
    return;
  };
  // 编辑ip
  if(clickEvent.hasClass('icon-point-edit')){
    $('.icon-point-edit').attr('title', '确定')
      .removeClass('icon-point-edit')
      .addClass('icon-point-sure')
      .next('.ip-input').removeAttr('readonly')
    ;
    return;
  };
  // 开关下拉列表
  if(clickEvent.hasClass('ip-input') && $ipInput.attr('readonly') == 'readonly'){
    if($('.IP-select li').length > 1) $ipSelect.fadeToggle();
    return;
  };
  // 清空端口
  if(clickEvent.hasClass('del-port')){
    clickEvent.next('.port-input').val('');
    return;
  };
  // 清空地址
  if(clickEvent.hasClass('del-addr')){
    clickEvent.next('.addr-input').val('');
    return;
  };
});

// 关闭弹层
doc.on('click', function(e){
  var clickEvent = $(e.target);
  // 颜色修改
  if(!clickEvent.is('.color-input') && !clickEvent.is('.icon-point-color')){
    $colorBox.fadeOut();
  };
  // ip修改
  if(!clickEvent.is('.IP-select li') && !clickEvent.is('.ip-input')){
    $ipSelect.fadeOut();
  };
});

// 下拉列表
doc.on('click', '.ip-select li', function(e){
  $ipInput.val($(this).attr('data-type'));
  // showQrcode(); // 刷新二维码
  $(this).parent().fadeOut();
  e.stopPropagation();
});

// 打开文件选择框
doc.on('dblclick', '.addr-input', function(){
  if($startBtn.attr('data-type') === '1') return;  // 判断是否启动
  $(this).select();
  $("#fileDialog")
    .attr('nwworkingdir', api.getlocalStorage('app-info', 'addr')) // 读取缓存设置默认起始地址
    .trigger('click'); // 模拟点击
});

// 监控文件修改
doc.on('change', '#fileDialog', function(e){
  $addrInput.val(this.value);
});

// 启动按钮事件
$startBtn.on({
  click: function(){
    console.info(
      '端口号：' + $portInput.val(),
      '按钮属性：' + $(this).attr('data-type')
    );
    var IP      = $ipInput.val()
      , Port    = $portInput.val()    // 端口号
      , Addr    = $addrInput.val()    // 项目地址
      , btnArrt = $(this).attr('data-type') // 按钮属性
      , bgColor = $colorInput.val()   // 颜色
    ;
    showQrcode(); // 刷新二维码
    // 项目地址不能为空
    if(!Addr || api.isEmptyDir(Addr) === false){
      api.toast('调试文件夹错误');
      $addrInput.focus();
      return;
    };
    // 启动服务
    if(btnArrt == 0 && IP !== '' && Port !== '' && Addr !== ''){
      $(this).attr('data-type', 1).text('运行中...').css('letter-spacing','0');

      // 禁止输入操作
      $portInput.attr('readonly', 'readonly');
      $addrInput.attr('readonly', 'readonly');
      // 写数据
      api.setlocalStorage('app-info', 'IP', IP);
      api.setlocalStorage('app-info', 'Port', Port);
      api.setlocalStorage('app-info', 'addr', Addr);
      api.setlocalStorage('app-info', 'bgColor', bgColor); // 修改背景颜色
      api.setlocalStorage('app-info', 'timestamp', api.timestamp());
      api.toastShow('启动中请稍等...');
      setTimeout(function(){
        require('./assets/js/app').appStart(Port, Addr);
      }, 100);
    }
    // 关闭服务
    else if(btnArrt == 1){
      global.mainWindow.reloadDev(); // 重新加载当前页从头开始一个新的渲染过程
    };
  },
  mouseover: function(){
    var $this = $(this);
    $this.attr('data-type') === '1' && $this.text('停止').css('letter-spacing','5px');
  },
  mouseout: function(){
    var $this = $(this);
    $this.attr('data-type')  === '1' && $this.text('运行中...').css('letter-spacing','0');
  }
});

// 渲染数据   /////////////////////////////////////////

// 显示二维码
var IPData   = '' // 记录ip
  , Website  = '' // 完整地址
;
function showQrcode(){
  var Port     = $portInput.val()
    , IP       = $ipInput.val()
    , $QRcode  = $('.qrcode')
  ;
  if(IPData === IP) return; // 判断是否修改

  if(IP == '' || !api.isIP(IP)){
    api.toast('IP 地址填写不正确');
    return;
  };
  if(Port == '' || Port < 1000){
    api.toast('端口号填写不正确');
    $portInput.focus();
    return;
  };
  $QRcode.empty(); // 清除节点

  IPData   = IP; // 记录ip
  Website  = 'http://' + IP + (Port ===''? '' : ':' + Port); // 完整地址

  // 绘制二维码
  $QRcode.qrcode({
    foreground: "#d81b1b",
    background: "#FFF",
    width: 100,
    height:100,
    text: Website
  });
};

