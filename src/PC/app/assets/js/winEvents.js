/**
 * winEvents.js
 */

'use strict';

var win      = global.mainWindow
  , gui      = global.gui
  , $        = global.jQuery
  , document = global.mainWindow.window.document
  , path     = require('path')
  , api      = require('./api')
;

// 托盘注册事件  /////////////////////////////////////////
var trayMenu  = new gui.Menu()
  , trayIcon  = path.resolve(__dirname, './../../../res/29x29.png')
  , tray      = new gui.Tray({icon: trayIcon})
  , $startBtn = $('.start-btn')
;

tray.tooltip = 'PPdebug - 设计稿实时预览工具';  // 托盘图标鼠标提示

// 托盘图标左键单击事件  /////////////////////////////////////////

tray.on('click', function(){
  if(!showWin){
    winMinimize(true);
  }
  else{
    winMinimize(false);
  }
});

// 托盘右键弹出菜单事件  /////////////////////////////////////////
tray.menu = trayMenu;

var menuItem = {
  item_1 : new gui.MenuItem({
    label: '  PPdebug',
    icon: trayIcon,
    click: function() {
      api.about(); // 打开关于PPdebug
    }
  }),

  item_2 : new gui.MenuItem({type: 'separator'}),

  item_3 : new gui.MenuItem({
    label: '打开浏览器',
    click: function() {
      // 判断启动按钮是否按下
      if($startBtn.attr('data-type') === '1'){
        var Port    = $('.port-input').val()
          , IP      = $('.ip-input').val()
          , Website = 'http://' + IP + ':' + Port
        ;
        gui.Shell.openExternal(Website); // 打开浏览器
      }
      else{
        winMinimize(true);
        api.toast('请启动后再操作');
      };
    }
  }),
  item_4 : new gui.MenuItem({
    label: '打开文件夹',
    click: function() {
      var addr = api.getlocalStorage('app-info', 'addr');
      if(addr){
        // 打开项目文件夹
        gui.Shell.openItem(
          path.normalize(addr)
        );
      }
      else{
        winMinimize(true);
        api.toast('调试文件夹路径错误');
      };
    }
  }),
  item_5 : new gui.MenuItem({
    label: '清空缓存',
    click: function() {
      // 判断是否启动
      if($startBtn.attr('data-type') === '0'){
        api.clearDir(api.tempDir()); // 文件缓存
        api.delLocalStorage('file-info'); // 删除文件修改缓存
      }
      else{
        winMinimize(true);
        api.toast('请停止后再操作');
      };
    }
  }),

  item_6 : new gui.MenuItem({type: 'separator'}),

  item_7 : new gui.MenuItem({
    label: '启动 / 停止',
    click: function() {
      // 判断是否启动
      if($startBtn.attr('data-type') === '0'){
        $(".start-btn").trigger('click'); // 模拟点击
      }
      else{
        global.mainWindow.reloadDev(); // 重新加载当前页从头开始一个新的渲染过程
      };
    }
  }),
  item_8 : new gui.MenuItem({
    label: '刷新调试端',
    click: function() {
      // 判断启动按钮是否按下
      if($startBtn.attr('data-type') === '1'){
        api.setlocalStorage('app-info', 'timestamp', api.timestamp());
      }
      else{
        winMinimize(true);
        api.toast('请启动后再操作');
      };
    }
  }),

  item_9 : new gui.MenuItem({type: 'separator'}),

  item_10 : new gui.MenuItem({
    label: '退出',
    click: function() {
      winClose();
    }
  })
};

// 生成菜单
for (var i in menuItem) trayMenu.append(menuItem[i]);


// 窗口事件   /////////////////////////////////////////

// 关闭窗口
function winClose(){
  tray.remove();
  win.hide();
  win.close();
};

module.exports.winClose = winClose;

// 最小化窗口
var showWin = true; //用来判断窗口是否显示和隐藏
function winMinimize(show){
  if(show){
    win.show();
    win.setShowInTaskbar(true); //是否在任务栏显示
    showWin = true;
  }
  else{
    win.hide();
    win.setShowInTaskbar(false); //是否在任务栏显示
    showWin = false;
  };
};

module.exports.winMinimize= winMinimize;

// 拖拽窗口   /////////////////////////////////////////
(function($win){
  var titleBar = document.getElementsByClassName('title-bar')[0] // 选择移动的区域
    , dragging = false
    , mouse_x
    , mouse_y
    , win_x
    , win_y
  ;

  titleBar.onmousedown = function(e){
    e = e.originalEvent || e;
    // 不需要拖动区域
    var isbg = $(e.target).closest('.icon-update, .win-btn').length < 1
    if (!isbg) return;
    dragging = true;
    mouse_x = e.screenX;
    mouse_y = e.screenY;
    win_x = win.x;
    win_y = win.y;
  };

  $win.onmousemove = function(e){
    if (!dragging) return;
    win.x = win_x + (e.screenX - mouse_x);
    win.y = win_y + (e.screenY - mouse_y);
  };

  $win.onmouseup=function () {
    dragging = false;
  };
})(window);

// 项目文件夹拖放事件   /////////////////////////////////////////
(function($win) {
  $win.ondragover = function(e){
    e.preventDefault();
    e.dataTransfer.dropEffect = 'move';
    return false;
  };

  $win.ondrop = function(e){
    var picture
      , stat
      , filesLength = e.dataTransfer.files.length
    ;
    e.preventDefault();
    for(var i = 0; i < filesLength; ++i){
      picture = e.dataTransfer.files[i].path;
      /*判断是否是文件*/
      stat = require("fs").lstatSync(picture);
      if(stat.isDirectory()) $('.addr-input').val(picture);
    };
    return false;
  };
}(window));
