/**
 * walkDir.js  遍历目录模块
 */

'use strict';

var fs       = require('fs')
  , path     = require('path')
  , config   = require('./config') // 配置文件
  , api      = require('./api') // 公共函数模块
  , PSD      = require('./PSDRread') // psd读取模块
  , fileList = []
;

/**
 * walkSync:同步遍历目录
 * @param {string} path:遍历路径
 * @param {string} isSubDir:是否遍历子目录
 * @return {array} 返回目录个文件路径数组
 */
function walkSync(path, isSubDir) {
  fileList = [];
  handleFileSync(path);

  fs.readdirSync(path).forEach(function(item) {
    var tmpPath = path + '/' + item;
    if (fs.statSync(tmpPath).isDirectory()) {
      // 是否遍历子目录
      if (isSubDir) {
        walkSync(tmpPath);
      }
      else {
        handleFileSync(tmpPath);
      };
    }
    else {
      handleFileSync(tmpPath);
    };
  })

  return fileList;
};

/**
 * handleFileSync:处理文件,判断是否是文件和文件夹
 * @param {string} path:遍历路径
 */
function handleFileSync(path) {
  path = path.replace(/\\/g, '/');

  if (fs.statSync(path).isDirectory()) {
    fileList.push({
      'isfile': 0,
      'url': path
    });
  }
  else {
    fileList.push({
      'isfile': 1,
      'url': path
    });
  };
};

exports.walkSync = walkSync;

/**
 * 获取图片
 * @param {String} FileAdd
 */
module.exports.picture = function (FileAdd){
  var imgUrl                     // 图片路径
    , suffix                     // 后缀名
    , imgGroup = []              // 图片组
    , dir                        // 目录
    , fileType = config.fileType // 获取文件类型
    , fileName                   // 文件名
    , fileList = walkSync(FileAdd, false)
  ;
  //目录遍历
  fileList.forEach(function(img){
    // 判断是否是文件
    if(img.isfile){
      imgUrl = img.url;
      suffix = path.extname(imgUrl).toLowerCase(); // 获取后最名，转化成小写；
      // console.log(imgUrl, FileAdd);
      // 循环匹配文件类型
      fileType.forEach(function(type){
        if(suffix === type){
          suffix !== '.psd' ? dir = './img': dir = './temp';
          if(suffix === '.psd') PSD.toPNG(imgUrl);
          fileName = path.basename(imgUrl);
          imgGroup.push({
            'name'      : fileName, // 获取文件名称
            'Url'       : dir + '/' + fileName + '?' + Date.parse(fs.statSync(imgUrl).ctime).toString(), // 获取文件地址
            'suffix'    : suffix,  // 获取文件后缀名
            'ctime'     : api.dateFormat(fs.statSync(imgUrl).ctime, 'yyyy-MM-dd hh:mm:ss'), // 格式化修改时间
            'animation' : (suffix === '.gif' ? api.IsAnimatedGif(FileAdd, fileName) : false) // 是否动画
          });
        };
      });
    };
  });
  return imgGroup;
};

/**
 * PSD预转换
 * @param {String} FileAdd
 */
module.exports.PreConvert = function (FileAdd){
  var imgUrl                     // 图片路径
    , suffix                     // 后缀名
    , fileType = config.fileType // 获取文件类型
    , fileList = walkSync(FileAdd, false)
  ;
  //目录遍历
  fileList.forEach(function(img){
      // 判断是否是文件
      if(img.isfile){
        imgUrl = img.url;
        suffix = path.extname(imgUrl).toLowerCase(); // 获取后最名，转化成小写；
        // 循环匹配文件类型
        fileType.forEach(function(type){
          if(suffix === type){
            if(suffix === '.psd') PSD.toPNG(imgUrl);
          };
        });
      };
  });
  api.toastHide(); // 隐藏提示
};