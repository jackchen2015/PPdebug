/**
 * PSDRread.js  PSD读取模块
 */

'use strict';

var fs   = require('fs')
  , path = require('path')
  , PSD  = require('psd') //psd读取模块
  , api  = require('./api')
;
/**
 * PSD转PNG
 * @param {String} imgUrl
 */
var dir    = api.tempDir()
  , hasDir = fs.existsSync(dir)
;
//console.log(dir);
// 判断目录是否存在
if(!hasDir) fs.mkdirSync(dir);

module.exports.toPNG = function (imgUrl){
//console.log(imgUrl);
  var fileCtime  = Date.parse(new Date(fs.statSync(imgUrl).ctime)).toString()// 获取修改时间
    , fileName   = path.basename(imgUrl) // 获取文件名
    , localCtime = api.getlocalStorage('file-info', fileName) // 获取缓存文件时间戳
    , tempFile   = dir + '\\' + path.basename(imgUrl, 'psd') + 'png' // 临时文件
  ;

  // 文件是否存在
  if(fs.existsSync(tempFile)){
    // 文件修改时间和缓存数据是否一致
    api.toastHide(); // 隐藏提示
    if(fileCtime !== localCtime){
      api.setlocalStorage('file-info', fileName, fileCtime + "?" + fileCtime);
      PSDtoPNG(imgUrl, tempFile);
    };
  }
  // 文件不存在
  else{
    api.setlocalStorage('file-info', fileName, fileCtime + "?" + fileCtime);
    PSDtoPNG(imgUrl, tempFile);
  };
};

/**
 * PSD转PNG
 * @param {String} imgUrl
 * @param {String} tempFile
 */
function PSDtoPNG(imgUrl, tempFile){
  PSD.open(imgUrl).then(function (psd) {
    return psd.image.saveAsPng(tempFile);
  }).then(function (){
    api.toastHide();  // 隐藏提示
    // console.log("PSD转PNG结束!" + imgUrl);
  });
};
