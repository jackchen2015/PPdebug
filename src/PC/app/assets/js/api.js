/**
 * api.js  公用方法模块
 */

'use strict';

global.localStorage = window.localStorage;

var fs   = require('fs')
  , path = require('path')
  , config = require('./config')
  , gui  = global.gui
  , $    = global.jQuery
;

/**
 * setStorage 设置本地缓存信息
 * @param {String} data 对应存储key
 * @param {String} key  对应存储value里的key
 * @param {String} val  对应存储value里的value
 */
module.exports.setlocalStorage = function (data, key, val){
  var cacheData
    , StorageData = global.localStorage.getItem(data)
  ;
//console.log(StorageData);
  StorageData ?
  cacheData = JSON.parse(StorageData) : cacheData = {};
  cacheData[key] = val;
  global.localStorage.setItem(data, JSON.stringify(cacheData));
};


/**
 * getStorage 获取本地缓存信息
 * @param {String} data 对应存储key
 * @param {String} key  对应存储value里的key
 * @return {String} 返回key对应的value值
 */
module.exports.getlocalStorage = function (data, key){
  var cacheData
    , StorageData = global.localStorage.getItem(data)
  ;
//console.log(StorageData);
  StorageData ?
  cacheData = JSON.parse(StorageData) : cacheData = '';
  return cacheData !== '' ? cacheData[key] : '';
};


/**
 * delLocalStorage 删除本地缓存信息
 * @param {String} dataName 对应存储key
 */
module.exports.delLocalStorage = function (dataName){
  global.localStorage.removeItem(dataName);
};


/**
 * isIP 验证ip是否正确
 * @param {String} ipNum
 */
function isIP(ipNum){
  var re=/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/;
  if(re.test(ipNum)){
    if(RegExp.$1<256 && RegExp.$2<256 && RegExp.$3<256 && RegExp.$4<256)
    return true;
  };
  return false;
};
module.exports.isIP = isIP;


/**
 * 格式化时间
 * @param  {Object} date date object
 * @param  {String} fmt  format string
 * @return {String}      result string
 * example: dateFormat(new Date(), "yyyy-MM-dd hh:mm:ss")
 */
module.exports.dateFormat = function (date, fmt) {
  var o = {
    "M+": date.getMonth() + 1,                   // 月份
    "d+": date.getDate(),                        // 日
    "h+": date.getHours(),                       // 小时
    "m+": date.getMinutes(),                     // 分
    "s+": date.getSeconds(),                     // 秒
    "q+": Math.floor((date.getMonth() + 3) / 3), // 季度
    "S" : date.getMilliseconds()                 // 毫秒
  };
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o)
  if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;
};


/**
 * 时间戳
 * @return string
 */
module.exports.timestamp = function (){
  return Math.round(new Date().getTime() / 100);
};


/**
 * 是否为GIF动画
 * @param {String} fileadd  文件路径
 * @param {String} fileName  文件名
 * @return Boolean
 */
module.exports.IsAnimatedGif = function (FileAdd, fileName){
  var openGIF = fs.readFileSync(FileAdd + '/' + fileName, 'utf-8');
  return (openGIF.indexOf('NETSCAPE2.0') >= 0 ? true : false);
};


/**
 * 自动消失提示框
 * @param {String} text
 */
module.exports.toast = function (text){
  var toast = $('.toast');
  if(toast.is(':hidden')){
    toast.html(text).fadeIn(1000);
    toast.fadeOut(2500);
  }else{
    return;
  };
};


/**
 * 显示提示框
 * @param {String} text
 */
module.exports.toastShow = function(text){
  var toast = $('.toast');
  if(toast.is(':hidden')) toast.html(text).fadeIn(1000);
};


/**
 * 隐藏提示框
 */
module.exports.toastHide = function(){
  var toast = $('.toast');
  if(!toast.is(':hidden')) toast.fadeOut(2500);
};


/**
 * 清空目录
 * @param {String} Path 路径
 */
module.exports.clearDir = function(Path){
  var hasDir = fs.existsSync(Path); // 判断文件夹是否存在
  if(hasDir){
    var dirList = fs.readdirSync(Path);
    // console.log(dirList.length);
    dirList.forEach(function(fileName){
      fs.unlinkSync(Path + '\\' +fileName);
    });
  }
};


/**
 * 目录是否为空
 * @param {String} Path 路径
 * @return Boolean
 */
module.exports.isEmptyDir = function(Path){
  var hasDir   = fs.existsSync(Path) // 判断文件夹是否存在
    , emptyDir = 0
  ;
  // console.info(hasDir);
  if(hasDir){
    emptyDir = fs.readdirSync(Path).length //判断目录是否为空
    return emptyDir > 0 ? true : false;
  }
  else{
    return false;
  };
};


/**
 * 临时文件目录
 * @return String
 */
module.exports.tempDir = function(){
  return path.resolve(__dirname, '../../showPage/temp/');
};


/**
 * 获取内网ip
 * @return String
 */
module.exports.getIPAdress = function() {
  var ifaces = require('os').networkInterfaces()
    , ip     = []
    , ipAddr
  ;
  for (var i in ifaces) {
    ipAddr = ifaces[i][1].address;
    if (ipAddr !== '127.0.0.1' && ipAddr !== 'localhost' && isIP(ipAddr)) {
      ip.push(ipAddr);
    };
  };
  return ip;
};


/**
 * 关于PPdebug
 */
module.exports.about = function(){
  gui.Shell.openExternal(config.infoUrl.about);
};


/**
 * 获取版本号
 */
module.exports.getVersion = function(){
  return fs.readFileSync(path.resolve(__dirname, './../../../version'),"utf-8");
};


/**
 * 打开下载地址
 */
module.exports.openDownload = function(){
  gui.Shell.openExternal(config.infoUrl.download);
};
