/**
 * config.js  配置文件模块
 */

// 支持文件类型
module.exports.fileType = [
    '.jpg'
  , '.jpeg'
  , '.bmp'
  , '.png'
  , '.gif'
  , '.svg'
  , '.psd'
];

// 信息地址
module.exports.infoUrl = {
  'about'    : 'http://www.kancloud.cn/pangxieju/ppdebug/200707',
  'download' : 'http://www.kancloud.cn/pangxieju/ppdebug/200841'
}
