'use strict';

//判断是否支持plus
window.plus ? plusReady():document.addEventListener('plusready',plusReady,false);
function plusReady(){
  console.log('屏幕常亮：'+plus.device.isWakelock());
  // 隐藏滚动条
  plus.webview.currentWebview().setStyle({scrollIndicator:'none'});
  // Android处理返回键
  plus.key.addEventListener('backbutton',function(){
    if(quitTap()){
      plus.runtime.quit();
    }else{
      plus.nativeUI.toast( '再按一次离开 PPdebug');
    };
  },false);

  /**
   * quitTap:退出函数，记录连续两次退出事件；
   */
  var lastTap,timeLong;
  function quitTap() {
    var nowTap = new Date(); //创建时间
    //判断是否为空
    if (!lastTap) {
      lastTap = nowTap;
      return false;
    }else{
      //获取经过时间长度
      timeLong = Math.round((nowTap.getTime() - lastTap.getTime()));
      if (timeLong > 500 && timeLong < 2500) {
        lastTap = nowTap;
        return true;
      }else{
        lastTap = new Date(); //重置开始时间
        return false
      };
    };
  };
};
getLink();/*读取存储链接*/

var doc = $(document);
//修改版本号
document.getElementById('version').innerHTML = 'v ' + version + ' Beta';
//编辑框获取焦点
doc.on('focus','.input-box input',function(){
  $(this).prev().show();
});
//编辑框失去焦点
doc.on('blur','.input-box input',function(){
  $(this).prev().hide();
});
//清空编辑框
doc.on('tap','.input-box .del-port',function(){
  $(this).next().val('');
});
//扫一扫
doc.on('tap','.QRcode',function(){
  plus.webview.create('QRcode_scan.html').show("pop-in");
});
//链接按钮单击
doc.on('tap','.row .link-btn',function(){
  var IP   = $('.ip-input').val()
    , Port = $('.port-input').val()
    , Url
  ;
  if(IP == '' || !isIP(IP)){
    plus.nativeUI.toast('IP 地址填写不正确');
    return;
  };
  if(Port == '' || Port < 1000){
    plus.nativeUI.toast('端口号填写不正确');
    return;
  };

  Url = 'http://' + IP + ':' + Port;

  console.log(Url);
  //判断地址是否正确
  $.ajax({
    type:"post",
    url:Url,
    dataType:'json',
    timeout:500,
    data:{sample:'payload'},
    success:function(data){
      saveLink(IP,Port);  //保存链接
      //创建子页面
      plus.webview.create(Url).show("pop-in");
    },
    error:function(xhr,type){
      //请求地址不正确提示
      plus.nativeUI.toast('IP或端口不正确');
    },
    async:true
  });
});
