/**
 * public.js  公用模块
 */

'use strict';

var version = '0.0.1'
  , Storage = localStorage
;

/**
 * 保存链接
 * @param {Object} IP
 * @param {Object} Port
 */
function saveLink(IPVal,PortVal){
  Storage.setItem( "IP", IPVal);
  Storage.setItem( "Port", PortVal);
};

/**
 * 删除链接
 */
function removeLink(){
  Storage.removeItem( "IP");
  Storage.removeItem( "Port");
};

/*
 * 读取存储信息
 */
function getLink(){
  if(Storage.getItem('IP') && Storage.getItem('Port')){
    $('.ip-input').val(Storage.getItem('IP'));
    $('.port-input').val(Storage.getItem('Port'));
  };
};

/**
 * 验证ip是否正确
 * @param {Object} ipNum
 */
function isIP(ipNum){
  var re=/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/;
  if(re.test(ipNum)){
    if(RegExp.$1<256 && RegExp.$2<256 && RegExp.$3<256 && RegExp.$4<256)
    return true;
  };
  return false;
};