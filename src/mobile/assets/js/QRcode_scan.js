'use strict';

var ws       = null
  , wo       = null
  , scan     = null
  , domready = false
  , bCancel  = false
  , doc      = document
;
//判断是否支持plus
window.plus ? plusReady():doc.addEventListener('plusready',plusReady,false);
// H5 plus事件处理
function plusReady() {
  if(ws || !window.plus || !domready) return;
  // 获取窗口对象
  ws = plus.webview.currentWebview();
  wo = ws.opener();
  // 开始扫描
  ws.addEventListener('show', function() {
    scan = new plus.barcode.Barcode('ScanArea', [plus.barcode.QR, plus.barcode.EAN8, plus.barcode.EAN13], {
      frameColor: '#d81b1b',
      scanbarColor: '#d81b1b'
    });
    scan.onmarked = onmarked;
    scan.start({
      conserve: true,
      filename: "_doc/barcode/"
    });
  });
  // 显示页面并关闭等待框
  ws.show("pop-in");
};
// 监听DOMContentLoaded事件
doc.addEventListener("DOMContentLoaded", function() {
  domready = true;
  plusReady();
}, false);
// 二维码扫描成功
function onmarked(type, result) {
  console.log('type:' + type);
  //二维码识别
  if(type === 0){
    plus.barcode.QR;
    type = "QR";
  };
  result = result.replace(/\n/g, ''); //去除换行符
  console.log(result);
  var ipSplit = result.replace(/http\:\/\//,'').split(':');
  wo.evalJS('getLink()');
  //判断链接是否是ip地址
  (ipSplit.length > 1) ? saveLink(ipSplit[0],ipSplit[1]) : removeLink();
  //打开链接
  plus.webview.create(result).show("pop-in");
  ws.close();
};

//闪光灯
$(doc).on('tap','.toolbar .bulb',function(){
  var dataType = $(this).attr('data-type')
    , stopBtn  = $('.toolbar .stop').attr('data-type')
  ;
  if(stopBtn == 1){
    plus.nativeUI.toast('点击"开始"后才能开灯');
    return;
  };
  //开灯
  if(dataType == 0){
    scan.setFlash(true);
    $(this).toggleClass('active').attr('data-type',1).find('.name').text('关灯');
    return;
  };
  //关灯
  if(dataType == 1){
    scan.setFlash(false);
    $(this).toggleClass('active').attr('data-type',0).find('.name').text('开灯');
    return;
  };
});
//扫描开关
$(doc).on('tap','.toolbar .stop',function(){
  var dataType = $(this).attr('data-type')
    , bulbBtn  = $('.toolbar .bulb')
  ;
  //停止扫描
  if(dataType == 0){
    $(this).toggleClass('active').attr('data-type',1).find('.name').text('开始');
    if(bulbBtn.attr('data-type') == 1){
      bulbBtn.removeClass('active').attr('data-type',0).find('.name').text('关灯');
    };
    scan.cancel();
    return;
  };
  //开始扫描
  if(dataType == 1){
    $(this).toggleClass('active').attr('data-type',0).find('.name').text('停止');
    scan.start({
      conserve: true,
      filename: "_doc/barcode/"
    });
    return;
  };
});
//打开相册
$(doc).on('tap','.toolbar .photo',function(){
  plus.gallery.pick(function(path) {
    plus.barcode.scan(path, onmarked, function(error) {
      plus.nativeUI.toast("无法识别此图片");
    });
  }, function(err) {
    plus.nativeUI.toast("错误: " + err.message);
  });
});
//退出
$(doc).on('tap','.toolbar .cancel',function(){
  ws.close();
});
