'use strict';

var gulp         = require('gulp')
  , del          = require('del') // 文件删除
  , uglify       = require('gulp-uglify') // js压缩
  , jshint       = require('gulp-jshint') //js检测
  , htmlmin      = require('gulp-htmlmin') // html压缩
  , minifyCss    = require('gulp-minify-css') // css压缩
  , notify       = require('gulp-notify') //提示信息
  , gulpSequence = require('gulp-sequence') //按顺序执行
  , childProcess = require('child_process')
;

gulp.task('default', function() {
  // 将你的默认的任务代码放在这
  console.log(' ◆  tasks help');
  console.log(' ---------------------------------');
  console.log(' ◆  gulp dist : Build `dist`');
  console.log(' ◆  gulp pack : Build `pack`');
  console.log(' ◆  gulp run  : Open `PPdebug.exe`');
  console.log(' ---------------------------------');
});

//////////////////////////////////////////////////////////////////////////////////////////

//打包
gulp.task('dist', gulpSequence(
  'del',
  'move',
  'htmlmin',
  'cssmin',
  'lint',
  'jsmin',
  'zip'
  )
);

//zip
var fs       = require('fs')
  , archiver = require('archiver')
  , archive  = archiver('zip')
;
gulp.task('zip', function() {
  //被打包文件
  var files = [
      'node_modules/**',
      'app/**',
      'res/**',
      'package.json',
      'version'
    ];
  
  var output = fs.createWriteStream('dist/PC/app.nw');
  archive.pipe(output);
  archive.bulk([
    {
    src: files,
    cwd: 'dist/PC/',
    expand: true
    }
  ]);
  archive.finalize();
})

// pack
gulp.task('pack', gulpSequence(
  'delPack',
  'movePack',
  'runPack'
 )
);

// 移动文件到pack目录
gulp.task('movePack', function(){
  // move file
  return gulp.src(
    [
      'dist/PC/icudtl.dat',
      'dist/PC/nw.pak',
      'dist/PC/使用说明.txt',
      'dist/PC/PPdebug.exe'
    ],
    {
      base: 'dist/PC'
    }
  )
    .pipe(gulp.dest('dist/PC/pack/'))
    .pipe(notify({ message: '\n task:move => folder:dist/PC/pack/ ok!'}));
})

var path = require('path');

// 打包文件
gulp.task('runPack', function() {
  var packPath = path.normalize(__dirname + '/dist/PC/pack/')
    , PCPath   = path.normalize(__dirname + '/dist/PC/') 
    , code     = 'copy /b ' + packPath + 'PPdebug.exe+' + PCPath + 'app.nw ' + packPath + 'PPdebug.exe'
  ; 
  console.log('run->pack ok!');
  childProcess.exec(code);
})

gulp.task('delPack', function () {
  console.log('task:delPack ok!');
  //删除文件
  return del(['dist/PC/pack/*']);
});

//删除src目录nwjs
gulp.task('del', function () {
  console.log('task:del ok!');
  //删除文件
  return del([
    'dist/PC/app/',
    'dist/PC/res/',
    'dist/PC/locales/',
    'dist/PC/app.nw',
//  'dist/PC/package.json',
    'dist/PC/使用说明.txt',
    'dist/PC/PPdebug.exe',
    'dist/PC/version'
  ]);
});

//移动到dist
gulp.task('move', function () {
  return gulp.src(
    [
      'src/PC/app/node_modules/express/',
      'src/PC/app/node_modules/psd/',
      'src/PC/app/assets/iconfont/*',
      '!src/PC/app/assets/iconfont/iconfont.css',
      'src/PC/res/*.png',
//    'src/PC/package.json',
      'src/PC/使用说明.txt',
      'src/PC/PPdebug.exe',
      'src/PC/version'
    ],
    {
      base: 'src'
    }
  )
    .pipe(gulp.dest('dist/'))
    .pipe(notify({ message: '\n task:move => folder:dist/PC/ ok!'}));
});

//压缩html
gulp.task('htmlmin', function () {
  var releasePath = 'dist/PC/';
  del(releasePath + 'app/**/*.html'); //清空目录下的html文件

  var options = {
        removeComments: true,//清除HTML注释
        collapseWhitespace: true,//压缩HTML
        collapseBooleanAttributes: true,//省略布尔属性的值 <input checked="true"/> ==> <input />
        removeEmptyAttributes: true,//删除所有空格作属性值 <input id="" /> ==> <input />
        removeScriptTypeAttributes: false,//删除<script>的type="text/javascript"
        removeStyleLinkTypeAttributes: false,//删除<style>和<link>的type="text/css"
        minifyJS: true,//压缩页面JS
        minifyCSS: true//压缩页面CSS
      };

  return gulp.src('src/PC/app/**/*.html')
    .pipe(htmlmin(options))
    .pipe(gulp.dest(releasePath + 'app/'))
    .pipe(notify({ message: '\n task:Htmlmin => folder: ok!'}));
});

//压缩css
gulp.task('cssmin', function () {
  var releasePath = 'dist/PC/';
  del(releasePath + 'app/**/*.css'); //清空目录下的css文件

  return gulp.src('src/PC/app/**/*.css')
    .pipe(minifyCss())
    .pipe(gulp.dest(releasePath + 'app/'))
    .pipe(notify({ message: '\n task:Cssmin => folder: ok!'}));
});

// 检查js
gulp.task('lint', function() {
  return gulp.src('src/PC/app/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(notify({ message: 'lint task ok' }));
});

//压缩js
gulp.task('jsmin', function () {
  var releasePath = 'dist/PC/';
  del(releasePath + 'app/**/*.js'); //清空目录下的js文件
  return gulp.src('src/PC/app/**/*.js')
    .pipe(uglify())
    .pipe(gulp.dest(releasePath + 'app/'))
    .pipe(notify({ message: '\n task:Jsmin => folder: ok!'}));
});

//运行PPdebug
gulp.task('run',function () {
  childProcess.exec(__dirname + '/dist/PC/PPdebug.exe');
});
