# PPdedug-0.20
帮助设计师高效端预览调试设计稿

* [帮助文档](http://www.kancloud.cn/pangxieju/ppdebug/200707)  
* [下载地址](http://www.kancloud.cn/pangxieju/ppdebug/200841)

## 特性
工具支持 windows 操作系统，支持jpg、jpeg、bmp、png、gif、svg、psd 图片文件在移动端多设备上进行预览，可边改边查看效果，提高设计师工作效率。

## 安装
```
npm install
```

## gulp

```
gulp         tasks help
gulp dist    Bild `dist`
gulp pack    Bild `pack`
gulp run     Open `PPdebug.exe` (dist/PC)
```

## 目录结构

- **主目录结构**

```
  ├── 主目录 
  │ ├── dist // 压缩编译的发布目录 
  │ └── src  // 源码目录

```

- **src目录结构**

```
  ├── src  
  │ ├── mobile // H5+ 可用 HBuider 打包安卓，苹果应用
  │ └── PC     // PC 客户端
  │   ├── app  // 核心文件
  │   │ ├── assets    // 静态文件
  │   │ ├── showPage  // 移动端访问目录  
  │   │ └── main.html // 界面 
  │   ├── loacles            // nw.js 目录，可删除
  │   ├── res                // logo文件目录
  │   ├── credits.html       // nw.js 文件，可删除
  │   ├── d3dcompiler_47.dll // nw.js 文件，可删除
  │   ├── ffmpegsumo.dll     // nw.js 文件，可删除
  │   ├── icudtl.dat         // nw.js 文件
  │   ├── libEGL.dll         // nw.js 文件，可删除
  │   ├── libGLESv2.dll      // nw.js 文件，可删除
  │   ├── nw.exe             // nw.js 文件
  │   ├── nw.pak             // nw.js 文件
  │   ├── nwjc.exe           // nw.js 文件
  │   ├── package.json
  │   ├── pdf.dll            // nw.js 文件，可删除
  │   ├── PPdebug.exe        // nw.js 文件修改过属性和图标
  │   ├── version            // 版本文件
  │   └── 使用说明.txt          // 版本升级记录
```

## 交流
* [新浪微博](http://weibo.com/332123861)    
* QQ交流群：139850352